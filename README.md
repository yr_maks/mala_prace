# Projekt SSP

## Build

### Eshop

1. Install dependencies `pip install -r requirements.txt`
2. Run server `python manage.py runserver`

### Docs

1. Install dependencies `pip install -r requirements-mkdocs.txt`
2. Build it `mkdocs build`
